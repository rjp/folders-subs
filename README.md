# Simple client to backup/restore UA2 folder subscriptions

## Install

    go get bitbucket.org/rjp/folders-subs

## Running

    go run foldersubs.go -u username -p password -f subs.json

Or run it without `-f` to output the JSON to `stdout`

    go run foldersubs.go -u username -p password | [something JSONy]