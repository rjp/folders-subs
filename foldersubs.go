package main

import (
    "os"
    "io/ioutil"
    "flag"
    "encoding/json"
    "bitbucket.org/rjp/gokwuan"
//    "bitbucket.org/rjp/edf"
)

type UAClient struct {
    botChan gokwuan.AbstractChan
    outputOK chan string
    currentState string
}

func newUAClient(bc gokwuan.AbstractChan) (UAClient) {
    tmp := UAClient{bc, make(chan string), "menu"}
    return tmp
}

// <reply="folder_list"><folder=1><name="test"/><accessmode=7/><subtype=1/><unread=1/></><folder=2><name="private"/><accessmode=263/><subtype=1/></><folder=3><name="chat"/><accessmode=7/><subtype=1/><temp=1/></><numfolders=3/></>

func cacheFolders(event gokwuan.ClientEvent, bot gokwuan.Client) {
    folders := event.Extra // bot.ConstructFolderList(event.Data)
    output, err := json.Marshal(folders)
    if err != nil {
        panic(err)
    } else {
        if bot.ExtraData["filename"] == "" {
            os.Stdout.Write(output)
        } else {
            ioutil.WriteFile(bot.ExtraData["filename"], output, 0777)
        }
    }
}

// Pending
func subscribedFolder(event gokwuan.ClientEvent) {
    q := event.Data.ValueMap()
    _ = q
}

// <announce="user_login"><userid=7116/><username="guest7116"/><status=1/><timeon=1448254306/><announcetime=1448254307/></>
func maybeRequestFolders(event gokwuan.ClientEvent, bot gokwuan.Client) {
    isItMe, e := event.Data.ValueOf("announce.username")
    if e != nil {
        panic(e)
    }
    if isItMe == bot.Username() {
        bot.RequestFolderList()
    }
}

func handleEvents(bot gokwuan.Client) {
    defer bot.ExtraWG.Done()

    for {
        event := <-bot.Events
        switch event.Event {
            case "announce_user_login": maybeRequestFolders(event, bot)
            case "reply_folder_subscribe": subscribedFolder(event)
            case "reply_folder_list": cacheFolders(event, bot); bot.SendControl("quit"); return
        }
    }
}

func main() {
    var username = flag.String("u", "", "-u username")
    var password = flag.String("p", "", "-p password")
    var filename = flag.String("f", "", "-f file")
    flag.Parse()

    if *username == "" || *password == "" {
        panic("Need a username or password")
    }

    bot := gokwuan.New(*username, *password)

    if *filename != "" {
        bot.ExtraData["filename"] = *filename
    }

    // This should probably be some kind of abstract wrapper.
    // Having to do this in your client code is a bit rubbish.
    bot.ExtraWG.Add(1)
    go handleEvents(bot)

    // Never returns
    bot.Login()
}
